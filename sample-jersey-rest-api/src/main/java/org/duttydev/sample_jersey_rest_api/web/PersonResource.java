package org.duttydev.sample_jersey_rest_api.web;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.duttydev.sample_jersey_rest_api.domain.Person;
import org.duttydev.sample_jersey_rest_api.service.PersonService;
import org.duttydev.sample_jersey_rest_api.web.util.PaginationParams;
import org.duttydev.sample_jersey_rest_api.web.util.PersonParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonResource.class);

	private final PersonService service;
	private final Executor executor;

	public PersonResource(@Autowired PersonService aService,@Qualifier("personsExecutor") Executor aExecutor) {
		service = aService;
		executor = aExecutor;
	}

	@GET
	public void getPersons(@Suspended AsyncResponse asyncResponse,@BeanParam PaginationParams params) throws InterruptedException, ExecutionException {
		executor.execute(() -> {
			LOGGER.info("Get paginated list of persons");
			service.getPersons(params).thenApply(asyncResponse::resume)
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				Response response = ex.getCause() != null && ex.getCause() instanceof IllegalArgumentException ?
						Response.status(Status.BAD_REQUEST).entity(ex).build() :
							Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build();

						return asyncResponse.resume(response);
			});
		});
	}

	@GET
	@Path("/{id}")
	public void get(@Suspended AsyncResponse asyncResponse,@PathParam("id") Long id) {
		executor.execute(() -> {
			service.getPerson(id)
			.thenApply(asyncResponse::resume)
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				return asyncResponse.resume(Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build());
			});
		});
	}

	@GET
	@Path("/search")
	public void search(@Suspended AsyncResponse asyncResponse,@BeanParam PersonParams params) throws InterruptedException, ExecutionException {
		executor.execute(() -> {
			LOGGER.info("Search for a list of persons");
			service.search(params)
			.thenApply(asyncResponse::resume)
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				Response response = ex.getCause() != null && ex.getCause() instanceof IllegalArgumentException ?
						Response.status(Status.BAD_REQUEST).entity(ex).build() :
							Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build();

						return asyncResponse.resume(response);
			});
		});
	}

	@POST
	public void create(@Suspended AsyncResponse asyncResponse,@Valid Person person) {
		executor.execute(() -> {
			service.create(person)
			.thenApply(asyncResponse::resume)
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				return asyncResponse.resume(Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build());
			});
		});
	}

	@PUT
	@Path("/{id}")
	public void put(@Suspended AsyncResponse asyncResponse,@PathParam("id") Long id,@Valid Person person) { 
		executor.execute(() -> {
			service.update(id, person)
			.thenApply(asyncResponse::resume)
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				return asyncResponse.resume(Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build());
			});
		});
	}

	@DELETE
	@Path("/{id}")
	public void delete(@Suspended AsyncResponse asyncResponse,@PathParam("id") Long id) {
		executor.execute(() -> {
			LOGGER.info("Deleting user with id {}",id);
			service.delete(id)
			.thenApply(o -> asyncResponse.resume(Response.ok().build()))
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				return asyncResponse.resume(Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build());
			});
		});
	}

	@POST
	@Path("/bulk")
	public void create(@Suspended AsyncResponse asyncResponse,@Valid List<Person> persons) {
		executor.execute(() -> {
			service.bulkCreate(persons)
			.thenApply(asyncResponse::resume)
			.exceptionally(ex -> {
				LOGGER.error(ex.getMessage());
				return asyncResponse.resume(Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).build());
			});
		});
	}

}
