package org.duttydev.sample_jersey_rest_api.web.util;

import javax.ws.rs.BeanParam;
import javax.ws.rs.QueryParam;

public class PersonParams {
	@QueryParam("firstName")
	public String firstName;
	@QueryParam("lastName")
	public String lastName;
	@QueryParam("emailAddress")
	public String emailAddress;
	
	@BeanParam
	public PaginationParams pageParams;
}
