package org.duttydev.sample_jersey_rest_api.web.util;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

public class PaginationParams {
	@QueryParam(value="page") @DefaultValue(value="0")
	public int page;
	
	@QueryParam(value="size") @DefaultValue(value="20")
	public int size;
	
	
}
