package org.duttydev.sample_jersey_rest_api.repo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;
import org.springframework.scheduling.annotation.Async;

@NoRepositoryBean
public interface BaseRepository<Entity> extends Repository<Entity, Long> {
	
	List<Entity> findAll();
	
	@Async
	CompletableFuture<Page<Entity>> findAll(Specification<Entity> spec,Pageable pageable);
	
	@Async
	CompletableFuture<Page<Entity>> findAll(Pageable pageable);
	
	@Async
	CompletableFuture<Entity> findOne(Long Long);

	@Async
	CompletableFuture<Entity> save(Entity e);
	
	@Async
	CompletableFuture<Void> delete(Long Long);

	@Async
	CompletableFuture<List<Entity>> save(Iterable<Entity> entities);
}