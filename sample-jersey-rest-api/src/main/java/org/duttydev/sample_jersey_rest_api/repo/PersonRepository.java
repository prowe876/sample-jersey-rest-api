package org.duttydev.sample_jersey_rest_api.repo;

import org.duttydev.sample_jersey_rest_api.domain.Person;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends BaseRepository<Person> {

	
}
