package org.duttydev.sample_jersey_rest_api.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.duttydev.sample_jersey_rest_api.domain.Person;
import org.duttydev.sample_jersey_rest_api.repo.PersonRepository;
import org.duttydev.sample_jersey_rest_api.web.util.PaginationParams;
import org.duttydev.sample_jersey_rest_api.web.util.PersonParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

	private final PersonRepository repo;
	
	@Autowired
	public PersonService(PersonRepository aRepo) {
		repo = aRepo;
	}
	
	public CompletableFuture<Page<Person>> getPersons(PaginationParams params) {
		return repo.findAll(new PageRequest(params.page, params.size, null));
	}
	
	public CompletableFuture<Page<Person>> search(PersonParams param) {
		Specifications<Person> searchSpec = null;

		if(param.firstName != null) {
			searchSpec = Specifications.where(getSearchSpec("firstName", param.firstName));
		}

		if(searchSpec != null && param.lastName != null) {
			searchSpec= searchSpec.or(getSearchSpec("lastName", param.lastName));
		} else if (param.lastName != null) {
			searchSpec = Specifications.where(getSearchSpec("lastName", param.lastName));
		}

		if(searchSpec != null && param.emailAddress != null) {
			searchSpec =  searchSpec.or(getSearchSpec("emailAddress", param.emailAddress));
		} else if (param.emailAddress != null) {
			searchSpec = Specifications.where(getSearchSpec("emailAddress", param.emailAddress));
		}

		return repo.findAll(searchSpec,new PageRequest(param.pageParams.page,param.pageParams.size));
	}
	
	public CompletableFuture<Person> getPerson(Long id) {
		return repo.findOne(id);
	}
	
	public CompletableFuture<Person> create(Person person) {
		return repo.save(person);
	}
	
	public CompletableFuture<List<Person>> bulkCreate(List<Person> persons) {
		return repo.save(persons);
	}
	
	public CompletableFuture<Person> update(Long id,Person person) {
		person.setId(id);
		return repo.save(person);
	}
	
	public CompletableFuture<Void> delete(Long id) {
		return repo.delete(id);
	}
	
	private Specification<Person> getSearchSpec(String property,String searchKey) {
		return new Specification<Person>() {

			@Override
			public Predicate toPredicate(Root<Person> arg0, CriteriaQuery<?> arg1, CriteriaBuilder arg2) {
				return arg2.like(arg0.get(property), "%"+searchKey+"%");
			}
		};
	}

}
