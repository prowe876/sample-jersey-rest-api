package org.duttydev.sample_jersey_rest_api;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.duttydev.sample_jersey_rest_api.domain.Address;
import org.duttydev.sample_jersey_rest_api.domain.Person;
import org.duttydev.sample_jersey_rest_api.repo.PersonRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Autowired
	private PersonRepository personRepo;

	@Override
	public void run(String... arg0) throws Exception {
		DataFactory df = new DataFactory();

		List<Person> randomPersons = IntStream.range(0, 501)
				.mapToObj(i -> {
					Person p = new Person();
					p.setFirstName(df.getFirstName());
					p.setLastName(df.getLastName());
					p.setEmailAddress(df.getEmailAddress().replace(" ", ""));
					p.setPhoneNumber(new StringBuffer(df.getNumberText(10)).insert(3, "-").insert(7,"-").toString());

					Address address = new Address();
					address.setStreet(df.getAddress());
					address.setAddreess(df.getAddressLine2());
					address.setCity(df.getCity());
					p.setAddress(address);
					return p;
				}).collect(Collectors.toList());

		personRepo.save(randomPersons).get();
	}

}
