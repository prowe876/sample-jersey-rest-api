package org.duttydev.sample_jersey_rest_api.domain;

public interface IEntity {
	public Long getId();
	public void setId(Long newId);
}
