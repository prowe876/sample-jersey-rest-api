package org.duttydev.sample_jersey_rest_api.config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.glassfish.jersey.process.JerseyProcessingUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jersey.repackaged.com.google.common.util.concurrent.ThreadFactoryBuilder;

@Configuration
public class AsyncConfiguration {

	@Bean(name="personsExecutor",destroyMethod="shutdownNow")
	public Executor personsExecutor() {
		return Executors.newCachedThreadPool(new ThreadFactoryBuilder()
				.setNameFormat("persons"+"-exec-%d")
				.setUncaughtExceptionHandler(new JerseyProcessingUncaughtExceptionHandler())
				.build());
	}
}
