package org.duttydev.sample_jersey_rest_api.config;

import javax.ws.rs.ApplicationPath;

import org.duttydev.sample_jersey_rest_api.web.PersonResource;
import org.duttydev.sample_jersey_rest_api.web.filters.CORSResponseFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
@ApplicationPath("/api")
public class JerseyConfiguration extends ResourceConfig {
	public JerseyConfiguration() {
		register(PersonResource.class);
		register(CORSResponseFilter.class);
	}
}
