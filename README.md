# Sample Jersey Rest API #

This is a secured sample rest api using Jersey and Spring boot.

The resources were implemented to be asynchronous which provides a performance boost over
a typical synchronous blocking rest api solution.

The rest api is secured by using Spring Security Oauth 2.